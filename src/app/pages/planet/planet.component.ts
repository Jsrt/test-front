import { PlanetService } from './../../services/planet.service';
import { PageEvent } from '@angular/material/paginator';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-planet',
  templateUrl: './planet.component.html',
  styleUrls: ['./planet.component.scss']
})
export class PlanetComponent implements OnInit {

  data: any = {};
  pageIndex: number = 1;
  constructor(private planetService: PlanetService) { }

  ngOnInit(): void {
    this.getPlanet()
  }

  getPlanet() {
      this.planetService.getPlanets(this.pageIndex)
        .subscribe((data: any) => {
          this.data = data
        })
  }

  pageEvent(event: PageEvent) {
    this.pageIndex = (event.pageIndex + 1)
    
    this.planetService.getPlanets(this.pageIndex)
    .subscribe((data: any) => {
      this.data = data
    })
  }


}
