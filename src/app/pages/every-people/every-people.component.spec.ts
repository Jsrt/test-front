import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EveryPeopleComponent } from './every-people.component';

describe('EveryPeopleComponent', () => {
  let component: EveryPeopleComponent;
  let fixture: ComponentFixture<EveryPeopleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EveryPeopleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EveryPeopleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
