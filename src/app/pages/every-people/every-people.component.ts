import { PeopleService } from './../../services/people.service';
import { Component, OnInit } from '@angular/core';

interface IDS {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-every-people',
  templateUrl: './every-people.component.html',
  styleUrls: ['./every-people.component.scss']
})
export class EveryPeopleComponent implements OnInit {

  filter: string = '';
  data: any = {};
  ids: IDS[] = [
    {value: '', viewValue: 'Seleccione'},
  ];
  
  constructor(private peopleService: PeopleService) {
    let i = 1
    
    while(i <= 82) {
      this.ids.push({value: i.toString(), viewValue: i.toString()})
      i++;
    }

  }

  ngOnInit(): void {
    this.get()
  }


  get () {
    if (this.filter !== '') {
      this.peopleService.getPeopleForId(this.filter)
        .subscribe((data: any) => {
          this.data = data
        })
    }

  }

}
