import { PeopleService } from './../../services/people.service';
import { PageEvent } from '@angular/material/paginator';
import { Component, OnInit } from '@angular/core';

interface Order {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})

export class PeopleComponent implements OnInit {

  constructor(private peopleService: PeopleService) { 
    this.getAll()
  }

  data: any = {};
  dataSource: Array<any> = [];
  displayedColumns: string[] = ['nombre', 'altura', 'peso'];
  pageIndex: number = 1;

  order: Order[] = [
    {value: '', viewValue: 'Seleccione'},
    {value: 'nombre', viewValue: 'nombre'},
    {value: 'peso', viewValue: 'peso'},
    {value: 'altura', viewValue: 'altura'}
  ];

  filter: string = '';

  ngOnInit(): void {
  }

  getAll() {
    let sort = undefined

    if (this.filter !== '') {
      sort = this.filter;
    }

    this.peopleService.getPeople(sort, this.pageIndex)
      .subscribe((data: any) => {
        this.data = data
        this.dataSource = this.data.results
      })
  }

  pageEvent(event: PageEvent) {
    this.pageIndex = (event.pageIndex + 1)
    
    let sort = undefined

    if (this.filter !== '') {
      sort = this.filter;
    }

    this.peopleService.pagintation(sort, (event.pageIndex + 1))
      .subscribe((data: any) => {
        this.data = data
        this.dataSource = this.data.results
      })
  }

}
