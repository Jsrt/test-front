import { PlanetComponent } from './pages/planet/planet.component';
import { EveryPeopleComponent } from './pages/every-people/every-people.component';
import { HomeComponent } from './pages/home/home.component';
import { PeopleComponent } from './pages/people/people.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'planet',
    component: PlanetComponent
  },
  {
    path: 'people',
    component: PeopleComponent
  },
  {
    path: 'every-people',
    component: EveryPeopleComponent
  },
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
