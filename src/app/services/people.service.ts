import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  constructor(private http: HttpClient) { }

  urlService: string = '/people';


  getPeople(sort: any, pageIndex: number) {

    let params = new HttpParams();

    params = params.set('page', pageIndex.toString())

    if (sort !== undefined) {
      params = params.set('sort', sort)
    }

    return this.http.get(`${environment.baseURL}${this.urlService}/`, { params })
  }

  getPeopleForId(id: string) {
    return this.http.get(`${environment.baseURL}${this.urlService}/${id}`)
  }

  pagintation(sort: any, pageIndex: number) {

    let params = new HttpParams()
      
    params = params.set('page', pageIndex.toString())

    if (sort !== undefined) {
      params = params.set('sort', sort)
    }

    return this.http.get(`${environment.baseURL}${this.urlService}/`, { params })
  }

}
