import { environment } from './../../environments/environment.prod';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlanetService {

  constructor(private http: HttpClient) { }

  urlService: string = '/planets';

  getPlanets(page: number) {
    const params = new HttpParams()
      .set('page', page.toString())

    return this.http.get(`${environment.baseURL}${this.urlService}`, { params });
  }
}
